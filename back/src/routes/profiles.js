const express = require('express')
const router = express.Router()
const Profile = require('../models/profiles')
// const AdressSchema = require('../models/Profile')

//GET All
router.get('/', async (req, res) => {
    try {
        const profiles = await Profile.find()
        res.send(profiles)
    } catch (error) {
        res.status(500).send({message: error.message})
    }
});

//GET One
// router.get('/:id', getUser ,(req, res) => {
//     res.json(res.user);
// });

//POST One
router.post('/', async (req, res) => {
    const user = new Profile({
        firstname: req.body.firstname,
        lastname: req.body.lastname,
        avatarNb: req.body.avatarNb,
        age: req.body.age,
        nationality: req.body.nationality,
        job: req.body.job
    })

    try{
        const newUser = await user.save()
        res.status(201).send(newUser)
    } catch(error) {
        res.status(400).send({message: error.message})
    }
    res.send(`Creating a new user!`)
});

//UPDATE One
router.patch('/:id', async (req, res) => {
    // res.send(`Updating mister ${req.params.id}!`)
    try {
        const updatedUser = await Profile.findByIdAndUpdate(req.params.id, req.body, {
            new: true, // Return the modified document rather than the original
        });
        res.json(updatedUser)
    } catch(err) {
        res.status(400).send({message: err.message})
    }
});

//DELETE One
router.delete('/:id', getUser, async (req, res) => {
    try {
        await res.user.remove()
        res.json({data: req.params.id})
    } catch(err) {
        res.status(500).send({message: err.message})
    }
});

async function getUser(req, res, next) {
    let user
    try {
        user = await Profile.findById(req.params.id);
        if(user === null) {
            res.status(404).send({message: `User doesn't exist. Can't find him`})
        }
    } catch(err) {
        res.status(500).send({message: `${err.message}`})
    }

    res.user = user
    next()
}

module.exports = router