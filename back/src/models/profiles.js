const mongoose = require('mongoose')

const ProfilesSchema = new mongoose.Schema({
    firstname: {
        type: String,
        required: true
    },
    lastname: {
        type: String,
        required: true
    },
    avatarNb: {
        type: Number,
        required: true
    },
    age: {
        type: Number,
        required: true
    },
    nationality: {
        type: String,
        required: true
    },
    job: {
        type: String,
        required: true
    }
})

module.exports = mongoose.model('Profiles', ProfilesSchema)
// module.exports = mongoose.model('Adress', AdressSchema)