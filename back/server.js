require('dotenv').config()

const express = require('express')
const cors = require('cors')
const app = express()
const bp = require('body-parser')
const mongoose = require('mongoose')
// const mailjet = require('node-mailjet')

mongoose.connect(`${process.env.DATABASE_URL}`)
const db = mongoose.connection
db.on('error', (error) => { console.log(error)})
db.once('open', () => console.log('Connect to database'))

const port = process.env.PORT || 3003

// mailjet.apiConnect(
//   `${process.env.MJ_API_KEY}`,
//   `${process.env.MJ_API_SECRET}`
// )

app.use(bp.json())
app.use(bp.urlencoded({extended: true}))
app.use(cors({
    credentials: true,
    origin:'*'
}));

const swaggerJsdoc = require("swagger-jsdoc");
const swaggerUi = require("swagger-ui-express");

const options = {
    definition: {
      openapi: "3.0.0",
      info: {
        title: "Trombinoscope Express API with Swagger",
        version: "0.1.0",
        description:
          "This is a simple CRUD API application made with Express and documented with Swagger",
        contact: {
          name: "tromb",
          url: "soontebe",
          email: "info@email.com",
        },
      },
      servers: [
        {
          url: "http://localhost:3003",
        },
      ],
    },
    apis: ["./server.js"],
};
  
const specs = swaggerJsdoc(options);
app.use(
  "/api",
  swaggerUi.serve,
  swaggerUi.setup(specs)
);

//Creation de toutes les futures routes
const profilesRouter = require('./src/routes/profiles')

app.use('/profiles', profilesRouter);

app.listen(port, () => {
  console.log(`Started the server on port ${port}`)
})