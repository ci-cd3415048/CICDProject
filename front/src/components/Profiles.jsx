import React, { useEffect, useState } from "react";
import { Col, Button } from "reactstrap";
import { useDispatch, useSelector } from "react-redux";
import { deleteProfile, getAllProfiles } from "../redux/actions/profiles";
import ModalProfile from "./Modal";
import "../styles.css";
import { chooseAvatar } from "../chooseAvatar";
import Profile from "./Profile";
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";

const MySwal = withReactContent(Swal);

const ProfilCard = ({ profile, isSingleProfile, setProfile }) => {
  const dispatch = useDispatch();

  const handleSingleProfile = () => {
    dispatch({ type: "GET_PROFILE", data: profile._id });
    setProfile(!isSingleProfile);
  };

  const handleConfirmDelete = async (id) => {
    return MySwal.fire({
      icon: "question",
      text: "Êtes vous sûr(e) de vouloir supprimer ce velib ?",
      showCancelButton: true,
      cancelButtonText: "Annuler",
      confirmButtonText: "Confirmer",
      backdrop: false,
      customClass: {
        actions: "d-flex justify-content-between",
        cancelButton: "btn btn-outline-primary",
        confirmButton: "btn btn-outline-danger",
      },
      buttonsStyling: false,
    }).then(function (result) {
      if (result.value) {
        dispatch(deleteProfile(id));
        return result;
      }
    });
  };

  return (
    <div className="card">
      <img
        src={chooseAvatar(profile.avatarNb)}
        className="img-class mx-auto"
        alt="affiche film"
      />
      <h2>
        {profile.firstname} {profile.lastname}
      </h2>
      <h3>Age: {profile.age}</h3>
      <div className="d-flex mt-3" style={{ gap: "15px" }}>
        <Button
          className="btn btn-outline-primary"
          outline
          color="primary"
          onClick={() => handleSingleProfile()}
        >
          Plus d'informations
        </Button>
        <Button
          className="btn btn-outline-danger"
          outline
          color="primary"
          onClick={() => handleConfirmDelete(profile._id)}
        >
          Supprimer du trombinoscope
        </Button>
      </div>
    </div>
  );
};

const Profiles = (props) => {
  const profiles = useSelector((state) => state.profilesReducer.profiles);
  const dispatch = useDispatch();
  // const [isSingleProfile, setProfile] = useState(false);

  useEffect(() => {
    dispatch(getAllProfiles());
  }, [dispatch]);

  return (
    <>
      {profiles?.length > 0 ? (
        !props.isSingleProfile ? (
          <div className="container mt-4 ml-2">
            <Col md="12">
              <div className="cards-grid">
                {profiles.map((element) => {
                  return (
                    <ProfilCard
                      key={element._id}
                      profile={element}
                      isSingleProfile={props.isSingleProfile}
                      setProfile={props.setProfile}
                    />
                  );
                })}
              </div>
            </Col>
          </div>
        ) : (
          <Profile
            isSingleProfile={props.isSingleProfile}
            setProfile={props.setProfile}
            isOpen={props.isOpen}
            toggle={props.toggle}
            _id={props._id}
            handleModalOpening={props.handleModalOpening}
          />
        )
      ) : null}
      {props.isOpen && (
        <ModalProfile
          isOpen={props.isOpen}
          toggle={props.toggle}
          _id={props._id}
        />
      )}
    </>
  );
};

export default Profiles;
