import React from "react";
import { useSelector } from "react-redux";
import { Col, Button, Row } from "reactstrap";
import { chooseAvatar } from "../chooseAvatar";
import { FaArrowLeft } from "react-icons/fa";
import { Edit } from 'react-feather'
import ModalProfile from "./Modal";

const Profile = (props) => {
  //** TODO - Ici, n'afficher uniquement que les informations de la personne */
  const currentProfile = useSelector(
    (state) => state.profilesReducer.currentProfile
  );
  
  return (
    <>
      <div className="container">
        <Row>
          <Col md="12" className="d-flex justify-content-between">
            <div className="my-4">
              <Button
                className="btn-icon ml-1"
                outline
                color="primary"
                onClick={() => props.setProfile(!props.isSingleProfile)}
              >
                <FaArrowLeft size={16} />
              </Button>{" "}
              Revenir a la page principale
            </div>
            <div className="my-4">
              <Button
                className="btn-icon ml-1"
                outline
                color="primary"
                onClick={() => props.handleModalOpening(!props.isOpen, currentProfile?._id)}
              >
                <Edit size={16} />
              </Button>{" "}
              Modifier
            </div>
          </Col>
          <Col md="6" className="mt-2">
            <img src={chooseAvatar(currentProfile.avatarNb)} alt="img" />
          </Col>
          <Col md="6" className="mt-2">
            <div className="d-flex justify-content-between mt-2">
              <span className="fs-6">Nom</span>
              <span className="lead fs-6">{currentProfile?.firstname}</span>
            </div>
            <div className="d-flex justify-content-between mt-2">
              <span className="fs-6">Prénom</span>
              <span className="lead fs-6">{currentProfile?.lastname}</span>
            </div>
            <div className="d-flex justify-content-between mt-2">
              <span className="fs-6">Age</span>
              <span className="lead fs-6">{currentProfile?.age}</span>
            </div>
            <div className="d-flex justify-content-between mt-2">
              <span className="fs-6">Nationalité</span>
              <span className="lead fs-6">{currentProfile?.nationality}</span>
            </div>
            <div className="d-flex justify-content-between mt-2">
              <span className="fs-6">Poste</span>
              <span className="lead fs-6">{currentProfile?.job}</span>
            </div>
          </Col>
        </Row>
      </div>
      {props.isOpen && (
        <ModalProfile
          isOpen={props.isOpen}
          toggle={props.toggle}
          _id={props._id}
        />
      )}
    </>
  );
};

export default Profile;
