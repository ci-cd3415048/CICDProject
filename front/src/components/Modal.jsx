import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  Modal,
  ModalHeader,
  ModalBody,
  Button,
  Form,
  FormGroup,
  Col,
  Label,
  Input,
  Row,
} from "reactstrap";
import { createProfile, updateProfile } from "../redux/actions/profiles";
import { images } from "../images";
import '../styles.css'

const ModalProfile = (props) => {
  const dispatch = useDispatch();
  const profiles = useSelector((state) => state.profilesReducer.profiles);
  const [firstname, setFirstname] = useState("");
  const [lastname, setLastname] = useState("");
  const [age, setAge] = useState("");
  const [avatarNb, setavatarNb] = useState(0);
  const [nationality, setNationality] = useState("");
  const [job, setJob] = useState("");

  const loadValue = (_id) => {
    if (_id !== null && _id !== undefined) {
      const profile = profiles.find((ele) => ele._id === _id);
      setFirstname(profile.firstname);
      setLastname(profile.lastname);
      setAge(profile.age);
      setavatarNb(profile.avatarNb);
      setNationality(profile.nationality)
      setJob(profile.job)
    }
  };

  const textButton = (id) => {
    if (id) {
      return `Modifier`;
    } else {
      return `Créer`;
    }
  };

  const textTitle = (id) => {
    if (id) {
      return `Modification d'une personne`;
    } else {
      return `Ajout d'une nouvelle personne`;
    }
  };

  const handleModalOpening = () => {
    props.toggle(!props.isOpen, null);
  };

  const addOrUpdateProfile = () => {
    const state = {
      firstname,
      lastname,
      age,
      avatarNb,
      nationality,
      job
      // updated_at : (moment().unix() * 1000).toString(),
    };

    if (!props._id) {
      dispatch(createProfile(state));
    } else {
      dispatch(updateProfile(props._id, state));
    }
    handleModalOpening();
  };

  return (
    <Modal
      isOpen={props.isOpen}
      toggle={() => handleModalOpening()}
      onOpened={() => loadValue(props._id)}
      className="modal-dialog-top modal-lg"
      backdrop={false}
    >
      <Form onSubmit={() => addOrUpdateProfile()}>
        <ModalHeader toggle={handleModalOpening}>
          {textTitle(props._id)}
        </ModalHeader>
        <ModalBody>
          <Row>
            <Col className="d-inline-block" md="6" xl="6">
              <FormGroup tag={Col} md="12">
                <Label for="cityname">Prénom de la personne</Label>
                <Input
                  id="firstname"
                  placeholder="Saisissez votre prénom"
                  value={firstname}
                  onChange={(e) => setFirstname(e.target.value)}
                />
              </FormGroup>
              <FormGroup tag={Col}>
                <Label for="mb">Age de la personne</Label>
                <Input
                  id="mb"
                  type="number"
                  placeholder="1"
                  value={age}
                  onChange={(e) => setAge(e.target.value)}
                />
              </FormGroup>
              <FormGroup tag={Col} md="12">
                <Label for="nationalité">Nationalité</Label>
                <Input
                  id="firstname"
                  placeholder="Saisissez la nationalité"
                  value={nationality}
                  onChange={(e) => setNationality(e.target.value)}
                />
              </FormGroup>
            </Col>
            <Col className="d-inline-block" md="6" xl="6">
              <FormGroup tag={Col} md="12">
                <Label for="stationname">Nom de la personne</Label>
                <Input
                  id="lastname"
                  placeholder="Saisissez votre nom"
                  value={lastname}
                  onChange={(e) => setLastname(e.target.value)}
                />
              </FormGroup>
              <FormGroup tag={Col} md="12">
                <Label for="job">Poste en cours</Label>
                <Input
                  id="job"
                  placeholder="Saisissez la fonction exercée"
                  value={job}
                  onChange={(e) => setJob(e.target.value)}
                />
              </FormGroup>
            </Col>
            <Col className="d-inline-block" md="12">
              <FormGroup tag={Col} md="12">
                <Label>Selectionner l'avatar souhaité</Label>
                <div className="d-flex" style={{gap:'30px'}}>
                  {images.map((e) => {
                    return <img src={e.img} alt="ime" width='50px' height='50px' className="img-section" onClick={() => setavatarNb(e.value)}/>;
                  })}
                </div>
              </FormGroup>
            </Col>
            <Col className="d-inline-block" md="12">
              <Button color="primary" type="submit">
                {textButton(props._id)}
              </Button>
            </Col>
          </Row>
        </ModalBody>
      </Form>
    </Modal>
  );
};

export default ModalProfile;
