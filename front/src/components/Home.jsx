import React, { useState } from "react";
import Profiles from "./Profiles";
import { Button } from "reactstrap";
import { Edit } from "react-feather";

const Home = (props) => {
  const [isOpen, toggle] = useState(false)
  const [key, setKey] = useState('')

  const handleModalOpening = (val, _id = null) => {
    toggle(val)
    setKey(_id)
  }

  const [isSingleProfile, setProfile] = useState(false);

  return (
    <div>
      <div className="text-center lead fs-2 mt-2">Trombinoscope REACT APP</div>
      {!isSingleProfile && (
        <div className="text-center my-4">
          <Button
            className="btn-icon ml-1"
            outline
            color="primary"
            onClick={() => handleModalOpening(!isOpen)}
          >
            <Edit size={16} />
          </Button>{" "}
          Ajouter une nouvelle personne
        </div>
      )}
      <Profiles
        isOpen={isOpen}
        toggle={handleModalOpening}
        isSingleProfile={isSingleProfile}
        setProfile={setProfile}
        _id={key}
        handleModalOpening={handleModalOpening}
      />
    </div>
  );
};

export default Home;
