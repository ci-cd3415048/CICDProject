import img1 from './assets/images/avatar1.png'
import img2 from './assets/images/avatar2.png'
import img3 from './assets/images/avatar3.png'
import img4 from './assets/images/avatar4.png'
import img5 from './assets/images/avatar5.png'
import img6 from './assets/images/avatar6.png'
import img7 from './assets/images/avatar7.png'
import img8 from './assets/images/avatar8.png'
import img9 from './assets/images/avatar9.png'
import img10 from './assets/images/avatar10.png'

export const chooseAvatar = (num) => {
    switch(num) {
        case 1:
            return img1
        case 2:
            return img2
        case 3:
            return img3
        case 4:
            return img4
        case 5:
            return img5
        case 6:
            return img6
        case 7:
            return img7
        case 8:
            return img8
        case 9:
            return img9
        case 10:
            return img10
        default:
            return null
    }
}