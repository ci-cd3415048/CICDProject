import axios from 'axios';

axios.defaults.baseURL = `${process.env.REACT_APP_API_URL}`

export const getAllProfiles = () => {
    return async (dispatch) => {
      const res = await axios
        .get('/profiles');
      dispatch({
        type: 'GET_PROFILES',
        data: res.data,
      });
    }
}

export const createProfile = (profile) => {
  return async (dispatch) => {
    const res = await axios
      .post('/profiles', profile);
      dispatch({
          type: 'CREATE_PROFILE',
          data: res.data,
      });
  }
}

export const updateProfile = (id, profile) => {
  return async (dispatch) => {
    const res = await axios
      .patch(`/profiles/${id}`, profile);
      dispatch({
          type: 'UPDATE_PROFILE',
          data: res.data,
      });
  }
}

export const deleteProfile = (id) => {
  return async (dispatch) => {
    const res = await axios
      .delete(`/profiles/${id}`);
      dispatch({
          type: 'DELETE_PROFILE',
          data: res.data.data,
      });
  }
}