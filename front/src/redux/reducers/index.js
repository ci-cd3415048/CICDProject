import profilesReducer from './profiles'
import { combineReducers } from 'redux';

const rootReducers = combineReducers({
    profilesReducer
})

export default rootReducers;