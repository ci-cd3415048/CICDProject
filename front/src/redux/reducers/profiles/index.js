const initialState = {
  profiles: [],
  currentProfile: {},
};

const profileReducer = (state = initialState, action) => {
  switch (action.type) {
    case "CREATE_PROFILE":
      return { ...state, profiles: [...state.profiles, action.data] };
    case "GET_PROFILES":
      return { ...state, profiles: action.data };
    case "DELETE_PROFILE":
      const newest = state.profiles.filter((el) => el._id !== action.data);
      return { ...state, profiles: newest };
    case "UPDATE_PROFILE":
      const newDataUpdated = state.profiles.map((elem) => {
        if (elem._id === action.data._id) {
          return action.data;
        } else {
          return elem;
        }
      });
      return { ...state, profiles: newDataUpdated, currentProfile: action.data };
    case "GET_PROFILE":
      const profileFinded = state.profiles.find(
        (element) => element._id === action.data
      );
      return { ...state, currentProfile: profileFinded };
    default:
      return state;
  }
};

export default profileReducer;
