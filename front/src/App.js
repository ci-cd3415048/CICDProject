// import './App.css';
import { Routes, Route } from 'react-router-dom'
import Home from './components/Home';
import Profile from './components/Profile';
import { useState } from 'react';

function App() {
  return (
    <>
       <Routes>
        <Route
          exact
          path='/'
          element={<Home/>}
        />
      </Routes>
    </>
  );
}

export default App;
