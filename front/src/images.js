import img1 from './assets/images/avatar1.png'
import img2 from './assets/images/avatar2.png'
import img3 from './assets/images/avatar3.png'
import img4 from './assets/images/avatar4.png'
import img5 from './assets/images/avatar5.png'
import img6 from './assets/images/avatar6.png'
import img7 from './assets/images/avatar7.png'
import img8 from './assets/images/avatar8.png'
import img9 from './assets/images/avatar9.png'
import img10 from './assets/images/avatar10.png'

export const images = [
    {
        img: img1,
        value: 1
    },
    {
        img: img2,
        value: 2
    },
    {
        img: img3,
        value: 3
    },
    {
        img: img4,
        value: 4
    },
    {
        img: img5,
        value: 5
    },
    {
        img: img6,
        value: 6
    },
    {
        img: img7,
        value: 7
    },
    {
        img: img8,
        value: 8
    },
    {
        img: img9,
        value: 9
    },
    {
        img: img10,
        value: 10
    },
]